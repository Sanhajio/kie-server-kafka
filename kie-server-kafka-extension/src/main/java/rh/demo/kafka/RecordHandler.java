package rh.demo.kafka;

import java.util.function.Function;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface RecordHandler<K, V> extends Function<ConsumerRecord<K, V>, SignalEvent> {



}
