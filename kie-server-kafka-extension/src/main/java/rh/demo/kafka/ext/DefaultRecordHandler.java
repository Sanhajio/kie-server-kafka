package rh.demo.kafka.ext;

import rh.demo.kafka.RecordHandler;
import rh.demo.kafka.SignalEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public class DefaultRecordHandler implements RecordHandler<String, String> {

    @Override
    public SignalEvent apply(ConsumerRecord<String, String> consumerRecord) {
        long processInstanceId = consumerRecord.key() !=null ? Long.parseLong(consumerRecord.key()): -1;
        return new SignalEvent(processInstanceId, "kafkaMessage", consumerRecord.value());
    }
}
