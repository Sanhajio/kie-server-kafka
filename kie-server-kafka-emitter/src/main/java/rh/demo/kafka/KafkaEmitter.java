package rh.demo.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.jbpm.persistence.api.integration.EventCollection;
import org.jbpm.persistence.api.integration.EventEmitter;
import org.jbpm.persistence.api.integration.InstanceView;
import org.jbpm.persistence.api.integration.base.BaseEventCollection;

import java.util.Collection;

public class KafkaEmitter implements EventEmitter {

    KafkaProducer

    @Override
    public void deliver(Collection<InstanceView<?>> collection) {
        //no-op
    }

    @Override
    public void apply(Collection<InstanceView<?>> collection) {

    }

    @Override
    public void drop(Collection<InstanceView<?>> collection) {

    }

    @Override
    public EventCollection newCollection() {
        return new BaseEventCollection();
    }

    @Override
    public void close() {

    }
}